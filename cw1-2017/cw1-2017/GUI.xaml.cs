﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 08/10/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for GUI.xaml
    /// </summary>
    public partial class GUI : Window
    {
        Attendee attendee = new Attendee(); // Create new instance of Attendee Class called attendee

        public GUI()
        {
            InitializeComponent();
        }

        // Method to set the values of the attendee class
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            bool createAttendee = FormValidation(); // Get whether or not the form has passed validation checks

            if (createAttendee == true) // If the form has passed validation checks then add values to attendee
            {
                try
                {
                    attendee.FirstName = txtFirstName.Text;     // Set attendee.FirstName to the text in the FirstName textbox
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.SecondName = txtSecondName.Text;   // Set attendee.SecondName to the text in the SecondName textbox
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.AttendeeRef = Convert.ToInt32(txtAttendeeRef.Text);    // Convert the text in the AttendeeRef textbox to an int and set attendee.AttendeeRef to that int.
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.InstitutionName = txtInstitutionName.Text; // Set attendee.InstitutionName to the text in the InstitutionName textbox
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.ConferenceName = txtConferenceName.Text;   // Set attendee.ConferenceName to the text in the ConferenceName textbox
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.RegistrationType = cmbBoxRegistrationType.Text;    // Set attendee.RegistrationType to the text in the RegistrationType textbox
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.Paid = HasPaid();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.Presenter = IsPresenter();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }

                try
                {
                    attendee.PaperTitle = txtPaperTitle.Text;   // Set attendee.PaperTitle to the text in the PaperTitle textbox
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.ToString());  // Show messagebox containing exception
                }
            }
        }

        // Method to validate user input
        private bool FormValidation()
        {
            int result = 0; // Temporary variable to hold result of parse
            bool canConvert = int.TryParse(txtAttendeeRef.Text, out result);    // Try to parse the contents of txtAttendeeRef to an int and store the result - true or false

            if (txtFirstName.Text == "")    // Check if FirstName textbox is empty
            {
                MessageBox.Show("Please enter a First Name");   // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (txtSecondName.Text == "")  // Check if SecondName textbox is empty
            {
                MessageBox.Show("Please enter a Second Name");  // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (txtAttendeeRef.Text == "") // Check if AttendeeRef textbox is empty
            {
                MessageBox.Show("Please enter an Attendee Reference");  // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (canConvert != true)    // Check if AttendeeRef is an int
            {
                MessageBox.Show("Attendee Reference is not a number"); // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (Convert.ToInt32(txtAttendeeRef.Text) < 40000)  // Check if AttendeeRef is less than 40000
            {
                MessageBox.Show("Attendee Reference is too small"); // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (Convert.ToInt32(txtAttendeeRef.Text) > 60000)  // Check if AttendeeRef is greater than 60000
            {
                MessageBox.Show("Attendee Reference is too big");   // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (txtConferenceName.Text == "")  // Check if Conference textbox is empty
            {
                MessageBox.Show("Please enter a Conference Name");  // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (cmbBoxRegistrationType.Text == "") // Check if Registration Type has been selected
            {
                MessageBox.Show("Please choose a Registration Type");   // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (rdbPaidYes.IsChecked == false && rdbPaidNo.IsChecked == false) // Check if a radio button has been selected
            {
                MessageBox.Show("Please choose an option for paid");    // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (rdbPresenterYes.IsChecked == false && rdbPresenterNo.IsChecked == false)   // Check if a radio button has been selected
            {
                MessageBox.Show("Please choose an option for presenter");   // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (rdbPresenterYes.IsChecked == true && txtPaperTitle.Text == "") // Check if PresenterYes radio button is selected and PaperTitle textbox is empty
            {
                    MessageBox.Show("Paper title cannot be empty when the attendee is a presenter");   // Show messagebox with error
                    return false;   // Return false - validation has failed
            }
            else if (rdbPresenterNo.IsChecked == true && txtPaperTitle.Text != "") // Check if PresenterNo radio button is selected and PaperTitle textbox is not empty
            {
                MessageBox.Show("Paper title cannot contain a value when the attendee is not a presenter");   // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else
            {
                return true;    // Return true - validation has passed
            }
        }

        // Method to determine which radio button is selected in the paid group
        private bool HasPaid()
        {
            // Check whether or not the attendee has paid

            if (rdbPaidYes.IsChecked == true)
            {
                return true;    // Return true - the attendee has paid
            }
            else
            {
                return false;   // Return false - the attendee has not paid
            }
        }

        // Method to determine which radio button is selected in the presenter group
        private bool IsPresenter()
        {
            // Check whether or not the attendee is a presenter.

            if (rdbPresenterYes.IsChecked == true)
            {
                return true;    // Return true - the attendee is a presenter
            }
            else
            {
                return false;   // Return false - the attendee is not a presenter
            }
        }

        // Method to clear user input
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Text = "";     // Set FirstName textbox to be empty
            txtSecondName.Text = "";    // Set SecondName textbox to be empty
            txtAttendeeRef.Text = "";   // Set AttendeeRef textbox to be empty
            txtInstitutionName.Text = "";   // Set InstitutionName textbox to be empty
            txtConferenceName.Text = "";    // Set ConferenceName textbox to be empty
            cmbBoxRegistrationType.Text = "";   // Set RegistrationType combobox to be empty
            rdbPaidYes.IsChecked = false;   // Set radio button PaidYes to not be checked
            rdbPaidNo.IsChecked = false;    // Set radio button PaidNo to not be checked
            rdbPresenterYes.IsChecked = false;  // Set radio button PresenterYes to not be checked
            rdbPresenterNo.IsChecked = false;   // Set radio button PresenterNo to not be checked
            txtPaperTitle.Text = "";    // Set PaperTitle textbox to be empty
        }

        // Method to get the values stored in the attendee class
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Text = attendee.FirstName;     // Set FirstName textbox to the value of attendee.FirstName
            txtSecondName.Text = attendee.SecondName;   // Set SecondName textbox to the value of attendee.SecondName
            txtAttendeeRef.Text = attendee.AttendeeRef.ToString();  // Convert attendee.AttendeeRef to string and set AttendeeRef textbox to that string
            txtInstitutionName.Text = attendee.InstitutionName;     // Set InstitutionName textbox to the value of attendee.InstitutionName
            txtConferenceName.Text = attendee.ConferenceName;       // Set ConferenceName textbox to the value of attendee.ConferenceName
            cmbBoxRegistrationType.Text = attendee.RegistrationType;    // Set RegistrationType combobox to the value of attendee.RegistrationType
            if (attendee.Paid == true)
            {
                rdbPaidYes.IsChecked = true;   // Set radio button PaidYes to be checked
                rdbPaidNo.IsChecked = false;    // Set radio button PaidNo to not be checked
            }
            else
            {
                rdbPaidNo.IsChecked = true;    // Set radio button PaidNo to be checked
                rdbPaidYes.IsChecked = false;    // Set radio button PaidYes to not be checked
            }
            if (attendee.Presenter == true)
            {
                rdbPresenterYes.IsChecked = true;   // Set radio button PresenterYes to be checked
                rdbPresenterNo.IsChecked = false;   // Set radio button PresenterNo to not be checked
            }
            else
            {
                rdbPresenterNo.IsChecked = true;    // Set radio button PresenterNo to be checked
                rdbPresenterYes.IsChecked = false;   // Set radio button PresenterYes to not be checked
            }
            txtPaperTitle.Text = attendee.PaperTitle;   // Set PaperTitle textbox to the value of attendee.PaperTitle
        }

        // Method to generate an invoice
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            Invoice invoice = new Invoice();    // Create instance of Invoice called invoice

            invoice.lblNameDisplay.Content = attendee.FirstName + " " + attendee.SecondName;    // Set lblNameDispay to contain attendee.FirstName and attendee.SecondName
            invoice.lblInstitutionDisplay.Content = attendee.InstitutionName;   // Set lblInstitutionDispay to contain attendee.InstitutionName
            invoice.lblConferenceDisplay.Content = attendee.ConferenceName;     // Set lblConferenceDispay to contain attendee.ConferenceName
            invoice.lblPriceDisplay.Content = "£" + attendee.GetCost();   // Set lblPriceDispay to contain the result from attendee.GetCost()

            invoice.Show(); // Show invoice window
        }

        // Method to generate a certificate
        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            Certificate certificate = new Certificate();    // Create instance of Certificate called certificate

            if (attendee.Presenter == true) // Check if the attendee is presenting or not
            {
                certificate.txtBlockCertificate.Text = "This is to certify that " + attendee.FirstName + " " + attendee.SecondName + " attended " + attendee.ConferenceName + " and presented a paper entitled " + attendee.PaperTitle + ".";   // Set txtBoxCertificate to contain a message along with the name, conference and paper title of the attendee
            }
            else
            {
                certificate.txtBlockCertificate.Text = "This is to certify that " + attendee.FirstName + " " + attendee.SecondName + " attended " + attendee.ConferenceName + ".";   // Set txtBoxCertificate to contain a message along with the name and conference of the attendee
            }

            certificate.ShowDialog(); // Show certificate window
        }

        // Method to enable/disable the PaperTitle textbox
        private void UpdatePaperTitle()
        {
            if (rdbPresenterYes.IsChecked == true)
            {
                txtPaperTitle.IsEnabled = true;    // The attendee is a presenter, enable input into textbox PaperTitle
            }
            else
            {
                txtPaperTitle.IsEnabled = false;   // The attendee is not a presenter, disable input into textbox PaperTitle
                txtPaperTitle.Text = "";       // The attendee is not a presenter, clear the textbox PaperTitle
            }
        }

        // Method to update the PaperTitle textbox IsEnabled property
        private void rdbPresenterNo_Checked(object sender, RoutedEventArgs e)
        {
            UpdatePaperTitle();
        }

        // Method to update the PaperTitle textbox IsEnabled property
        private void rdbPresenterYes_Checked(object sender, RoutedEventArgs e)
        {
            UpdatePaperTitle();
        }
    }
}
