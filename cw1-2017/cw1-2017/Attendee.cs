﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about an attendee
// Last updated: 05/10/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cw1_2017
{
    public class Attendee : Person
    {
        private int attendeeRef;            // Store the attendee's reference number
        private string institutionName;     // Store the attendee's institution name
        private string conferenceName;      // Store the attendee's conference
        private string registrationType;    // Store the attendee's registration type
        private bool paid;                  // Store the attendee's paid status
        private bool presenter;             // Store the attendee's presenter status
        private string paperTitle;          // Store the attendee's paper title

        // Store the attendee's reference number
        public int AttendeeRef
        {
            get
            {
                return attendeeRef;     // Return the value of private variable attendeeRef
            }
            set
            {
                if (value < 40000)  // Check if value is less than 40000
                {
                    throw new ArgumentException("The attendee reference is too small!");    // If value is less than 40000, throw exception
                }
                else if (value > 60000) // Check if value is greater than 60000
                {
                    throw new ArgumentException("The attendee reference is too big!");  // If value is greater than 60000, throw exception
                }
                else
                {
                    attendeeRef = value;    // Set the value of private variable attendeeRef
                }
            }
        }

        // Store the attendee's institution name
        public string InstitutionName
        {
            get
            {
                return institutionName;     // Return the value of private variable institutionName
            }
            set
            {
                institutionName = value;    // Set the value of private variable institutionName
            }
        }

        // Store the attendee's conference
        public string ConferenceName
        {
            get
            {
                return conferenceName;  // Return the value of private variable conferenceName
            }
            set
            {
                if (value == "")    // Check if value is empty
                {
                    throw new ArgumentException("The conference name is empty!");    // If value is empty, throw exception
                }
                else
                {
                    conferenceName = value; // Set the value of private variable
                }
            }
        }

        // Store the attendee's registration type
        public string RegistrationType
        {
            get
            {
                return registrationType;    // Return the value of private variable registrationType
            }
            set
            {
                if (value == "")    // Check if value is empty
                {
                    throw new ArgumentException("The registration type is empty!");     // If value is empty, throw exception
                }
                //else if (value != "full" || value != "student" || value != "organiser") // Check if value is not equal to the required inputs
                //{
                //    throw new ArgumentException("The registration type must be either full, student or organiser!");    // If value is not equal to the required inputs, throw exception
                //}
                else
                {
                    registrationType = value;   // Set the value of private variable registrationType
                }
            }
        }

        // Store the attendee's paid status
        public bool Paid
        {
            get
            {
                return paid;    // Return the value of private variable paid
            }
            set
            {
                paid = value;   // Set the value of private variable paid
            }
        }

        // Store the attendee's presenter status
        public bool Presenter
        {
            get
            {
                return presenter;   // Return the value of private variable presenter
            }
            set
            {
                presenter = value;  // Set the value of private variable presenter
            }
        }

        // Store the attendee's paper title
        public string PaperTitle
        {
            get
            {
                return paperTitle;  // Return the value of private variable paperTitle
            }
            set
            {
                if (presenter == true)  // Check if presenter is true
                {
                    if (value == "")    // Check if value is empty
                    {
                        throw new ArgumentException("The paper title cannot be blank when presenter is true!");   // If value is empty, throw exception
                    }
                    else
                    {
                        paperTitle = value; // Set the value of private variable paperTitle
                    }
                }
                else
                {
                    if (value != "")    // Check if value is not empty
                    {
                        throw new ArgumentException("The paper title must be blank when presenter is false!");   // If value is not empty, throw exception
                    }
                    else
                    {
                        paperTitle = value; // Set the value of private variable paperTitle
                    }
                }
            }
        }

        // Return the cost of attending the confernece
        public int GetCost()
        {
            if (registrationType == "full") // Check if registrationType is full
            {
                if (presenter == true)  // Check if presenter is true
                {
                    return 450; // If presenter is true, then 10% discount is applied
                }
                else
                {
                    return 500; // Otherwise, full price is charged
                }
            }
            else if (registrationType == "student") // Check if registrationType is student
            {
                if (presenter == true)  // Check if presenter is true
                {
                    return 270; // If presenter is true, then 10% discount is applied
                }
                else
                {
                    return 300; // Otherwise, full price is charged
                }
            }
            else   // If we get to here then registrationType is organiser
            {
                return 0;
            }
        }
    }
}
