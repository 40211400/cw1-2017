﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about a person
// Last updated: 15/10/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    public class Person
    {
        private string firstName;   // Store the attendee's first name
        private string secondName;  // Store the attendee's second name

        // Store the attendee's first name
        public string FirstName
        {
            get
            {
                return firstName;   // Return the value of private variable firstName
            }
            set
            {
                if (value == "")    // Check if value is empty
                {
                    throw new ArgumentException("The first name is empty!");    // If value is empty, throw exception
                }
                else
                {
                    firstName = value;  // Set the value of private variable firstName
                }
            }
        }

        // Store the attendee's second name
        public string SecondName
        {
            get
            {
                return secondName;  // Return the value of private variable secondName
            }
            set
            {
                if (value == "")    // Check if value is empty
                {
                    throw new ArgumentException("The second name is empty!");    // If value is empty, throw exception
                }
                else
                {
                    secondName = value; // Set the value of private variable secondName
                }
            }
        }
    }
}
